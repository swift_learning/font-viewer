//
//  ViewController.swift
//  Font Viewer
//
//  Created by iulian david on 07/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    @IBOutlet weak var fontFamiliesPopup: NSPopUpButton!
    @IBOutlet weak var fontTypesPopup: NSPopUpButton!
    @IBOutlet weak var sampleLable: NSTextField!
    let fontManager = NSFontManager.shared
    
    
    var selectedFontFamily: String?
    var fontFamilyMembers = [[Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        populateFontFamilies()
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func handleFontFamilySelection(_ sender: Any) {
        handleFontFamily()
    }
    
    @IBAction func handleFontTypeSelection(_ sender: Any) {
        updateSampleText()
    }
    
    @IBAction func displayAllFonts(_ sender: Any) {
        let storyboardName = NSStoryboard.Name(stringLiteral: "Main")
        let storyboard = NSStoryboard(name: storyboardName, bundle: nil)
        
        let storyboardID = NSStoryboard.SceneIdentifier(stringLiteral: "fontsDisplayWindowController")
        if let fontsDisplayWindowController = storyboard.instantiateController(withIdentifier: storyboardID) as? NSWindowController {
            
            if let vc = fontsDisplayWindowController.contentViewController as? FontsDisplayViewController {
                vc.fontFamily = selectedFontFamily
                vc.fontFamilyMembers = fontFamilyMembers
            }
            
            fontsDisplayWindowController.showWindow(nil)
        }
    }
    
    fileprivate func setupUI() {
        // Do any additional setup after loading the view.
        sampleLable.stringValue = """
        A beautiful text written with some font
        """
        sampleLable.alignment = .center
        
        fontFamiliesPopup.removeAllItems()
        fontTypesPopup.removeAllItems()
    }
    
    fileprivate func populateFontFamilies() {
        fontFamiliesPopup.addItems(withTitles: fontManager.availableFontFamilies)
        handleFontFamilySelection(self)
    }
    
    private func handleFontFamily() {
        if let fontFamily = fontFamiliesPopup.titleOfSelectedItem {
            selectedFontFamily = fontFamily
            if let members = fontManager.availableMembers(ofFontFamily: fontFamily) {
                fontFamilyMembers.removeAll()
                fontFamilyMembers = members
                updateFontTypePopup()
                view.window?.title = fontFamily
            }
        }
    }
    
    private func updateSampleText() {
        let selectedMember = fontFamilyMembers[fontTypesPopup.indexOfSelectedItem]
        if let postscriptName = selectedMember[0] as? String,
            let weight = selectedMember[2] as? Int,
            let traits = selectedMember[3] as? UInt,
            let fontFamily = selectedFontFamily {
            let font = fontManager.font(withFamily: fontFamily, traits: NSFontTraitMask(rawValue: traits), weight: weight, size: 19.0)
            sampleLable.font = font
            sampleLable.stringValue = postscriptName
        }
    }
    
    func updateFontTypePopup() {
        fontTypesPopup.removeAllItems()
        fontTypesPopup.addItems(withTitles: fontFamilyMembers.compactMap { $0[1] as? String } )
        fontTypesPopup.selectItem(at: 0)
        handleFontTypeSelection(self)
    }
}

