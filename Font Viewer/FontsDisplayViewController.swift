//
//  FontsDisplayWindowController.swift
//  Font Viewer
//
//  Created by iulian david on 07/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Cocoa

class FontsDisplayViewController: NSViewController {
    
    var fontFamily: String?
    var fontFamilyMembers = [[Any]]()
    

    @IBOutlet weak var fontsTextview: NSTextView!
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
        //        fontsTextview.string = fontFamilyMembers.compactMap { $0[0] as? String }.joined(separator: ",")
        setupTextView()
        showFonts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
       
    }

    func setupTextView() {
        fontsTextview.backgroundColor = NSColor(white: 1.0, alpha: 0.0)
        fontsTextview.enclosingScrollView?.backgroundColor = NSColor(white: 1.0, alpha: 0.0)
        fontsTextview.isEditable = false
        fontsTextview.enclosingScrollView?.autohidesScrollers = true
    }
    
    @IBAction func closeWindow(_ sender: Any) {
        view.window?.close()
    }
    
    func showFonts() {
        guard let fontFamily = fontFamily else { return }
        
        var lengths = [Int]()
        let fontPostscriptNames = fontFamilyMembers
            .compactMap { arr in
                guard let str = arr[0] as? String else {
                    return nil
                }
                lengths.append(str.count)
                return str }
            .joined(separator: "\n")
        let attributedString = NSMutableAttributedString(string: fontPostscriptNames)
        
        for (index, member) in fontFamilyMembers.enumerated() {
            if let weight = member[2] as? Int, let traits = member[3] as? UInt {
                if let font = NSFontManager.shared.font(withFamily: fontFamily, traits: NSFontTraitMask(rawValue: traits), weight: weight, size: 19.0) {
                   var location = 0
                    if index > 0 {
                        for i in 0..<index {
                            location += lengths[i] + 1
                        }
                    }
                    let range = NSMakeRange(location, lengths[index])
                    attributedString.addAttribute(.font, value: font, range: range)
                }
                
            }
        }
        attributedString.addAttribute(.foregroundColor, value: NSColor.white, range: NSMakeRange(0, attributedString.string.count))
        
        fontsTextview.textStorage?.setAttributedString(attributedString)
    }
}
